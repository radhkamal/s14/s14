// console.log("Hello World!");

let firstName = "Parveen", lastName = "Kaur", age = 33;

let hobbies = ["microbiology research", "genetics research", "studying"];

let workAddress = {
	houseNumber: 618,
	street: "Okanagan Lake",
	city: "Kelowna",
	state: "Okanagan Region",
	province: "British Columbia",
	country: "Canada"
}

console.log("First Name: " + firstName);
console.log("Last Name: " + lastName);
console.log("Age: " + age);
console.log("Hobbies:");
console.log(hobbies);
console.log("Work Address:");
console.log(workAddress);

function printUserInfo(firstName, lastName, age) {

	let introduction = firstName + " " + lastName + " is " + age + " years of age and is a Canadian actress known for playing the character of Dr. Saanvi Bahl in the Netflix series \"Manifest\".";

	let paragraph = "This was printed inside of the function \n";

	console.log(introduction);
	console.log(paragraph);
	console.log(hobbies);
	console.log(paragraph);
	console.log(workAddress);
}


printUserInfo(firstName, lastName, age);
